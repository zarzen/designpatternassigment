package zz.lab;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Vector;
import java.io.*;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		final HashMap<String,String> map = new HashMap<String,String>();
		Vector<String> mailType = new Vector<String>();
		
		System.out.println(System.getProperty("user.dir"));
		
		InputStream is = new FileInputStream("src/zz/lab/MailType");
		String line; // 用来保存每行读取的内容
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		line = reader.readLine(); // 读取第一行
	    while (line != null) { // 如果 line 为空说明读完了
	        System.out.println(line);
	        String[] ssStrings = line.split(" ");
	        
	        for (int i = 0; i < ssStrings.length; i=i+2) {
				System.err.println(ssStrings[i]);
				map.put(ssStrings[i], ssStrings[i+1]);
				mailType.addElement(ssStrings[i]);
			}
	        
	        line = reader.readLine(); // 读取下一行
	    }
	    reader.close();
	    is.close();
	    
		
		JFrame f=new JFrame("JComboBox1");
		Container contentPane=f.getContentPane();
		contentPane.setLayout(new GridLayout(1,2));
		
		    
		final JComboBox combo1=new JComboBox(mailType);
		combo1.setBorder(BorderFactory.createTitledBorder("选择邮件类型"));
		    

		final JTextField textfield_a = new JTextField();
		final JTextField textfield_b = new JTextField();
		final JTextField textfield_c = new JTextField();
		textfield_a.setBorder(BorderFactory.createTitledBorder("包裹重量"));
		textfield_b.setBorder(BorderFactory.createTitledBorder("邮寄距离"));
		textfield_c.setBorder(BorderFactory.createTitledBorder("保价金额"));
		
		contentPane.add(combo1);
		contentPane.add(textfield_a);
		contentPane.add(textfield_b);
		contentPane.add(textfield_c);
		
		
		final JButton kick = new JButton();
		kick.setText("计算");
		kick.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (e.getSource() == kick) {
					String getFromCombo = combo1.getSelectedItem().toString();
					
					float _a = Float.valueOf(textfield_a.getText().trim()).floatValue();
					float _b = Float.valueOf(textfield_b.getText().trim()).floatValue();
					float _c = Float.valueOf(textfield_c.getText().trim()).floatValue();
					try {
						
						
						Class<?> mailClass = Class.forName(map.get(getFromCombo).toString());
						Mail mail = (Mail)mailClass.newInstance();
						mail.setA(_a);
						mail.setB(_b);
						mail.setC(_c);
						SendPrice price = (SendPrice)mail;
		
						kick.setText(price.price());

					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (InstantiationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});

		contentPane.add(kick);

		f.pack();
		f.show();
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		
	}

}
