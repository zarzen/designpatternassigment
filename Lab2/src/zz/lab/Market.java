package zz.lab;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class Market implements Subject{
	private Vector<Bill> bills;
	private Vector<Observer> observers;
	
	private Timer markeTimer = new Timer();
	
	private Stock oneStock;//本来应该模拟多个股票，那么这里是个vector，现在简单起见
	private int stockAllShares;//真实情况不需要这样记录，因为那个是股票市场的原理。这里需要记录，以免交易总量超过股票发行总量。
	
	
	private class marketSim extends TimerTask{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("simulate once");
			double decision = Math.random()-0.5;
			if (decision >0) {
				System.out.println("generate one bill");
				double todayPrice = oneStock.getTodayPrice();
				double randomRate = Math.random()*0.2-0.1;//涨幅
				double transPrice = todayPrice*(1+randomRate);
				
				int transVolume = (int)Math.random()*stockAllShares;
				stockAllShares = stockAllShares-transVolume;
				
				Bill bill = new Bill();
				bill.setStock(oneStock);
				bill.setTransactionPrice(transPrice);
				bill.setVolume(transVolume);
				bill.setTransactionTime(new Date());
				addBill(bill);
				
			}else{
				System.out.println("do not generate on bill");
			}
		}

	}
	private class stopTime extends TimerTask{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			stop();
		}
		
	}
	
	public void start() {
		markeTimer.schedule(new marketSim(), 0,1*1000);//10s 模拟一次交易
		markeTimer.schedule(new stopTime(), 10*1*1000);
	}
	
	public void stop() {
		markeTimer.cancel();
	}
	
	public Market() {
		// TODO Auto-generated constructor stub
		bills = new Vector<Bill>();
		observers = new Vector<Observer>();
		oneStock = new Stock("601857", "中国石油", 1000000000, 7.51);
		stockAllShares = oneStock.getSharesNum();
		
	}
	
	public void addBill(Bill bill){
		bills.add(bill);
		notifyOberver();
	}
	

	@Override
	public void registerObserver(Observer observer) {
		// TODO Auto-generated method stub
		observers.add(observer);
	}

	@Override
	public void removeObserver(Observer observer) {
		// TODO Auto-generated method stub
		observers.remove(observer);
	}

	@Override
	public void notifyOberver() {
		// TODO Auto-generated method stub
		
		for (Observer element : observers) {
			
			element.update(bills);
			
		}
	}

}
