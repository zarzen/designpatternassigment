package zz.lab;

public class Stock {
	private String code;
	private String company;
	private int sharesNum;
	private double todayPrice;
	
	public double getTodayPrice() {
		return todayPrice;
	}

	public void setTodayPrice(double todayPrice) {
		this.todayPrice = todayPrice;
	}

	public Stock(String code, String company, int i,double d) {
		// TODO Auto-generated constructor stub
		this.code = code;
		this.company = company;
		this.sharesNum = i;
		this.todayPrice = d;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public int getSharesNum() {
		return sharesNum;
	}
	public void setSharesNum(int sharesNum) {
		this.sharesNum = sharesNum;
	}
	
	
	

}
