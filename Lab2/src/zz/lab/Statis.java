package zz.lab;

import java.util.Vector;

public class Statis implements Observer{
	protected Vector<Bill> bills;
	protected Subject market;
	
	public Statis(Subject market) {
		// TODO Auto-generated constructor stub
		this.market = market;
		market.registerObserver(this);
		
	}
	public void observer(Subject subject) {
		market = subject;
		market.registerObserver(this);
	}
	

	@Override
	public void update(Vector<Bill> bills) {
		// TODO Auto-generated method stub
		this.bills = bills;
		display();
	}
	
	private void display() {
		System.out.println(bills.toString());
		Bill lastBill = bills.lastElement();
		System.out.println("最后交易价："+lastBill.getTransactionPrice());
		System.out.println("今天开盘价："+lastBill.getStock().getTodayPrice());
		double highestPrice = lastBill.getStock().getTodayPrice();
		for (Bill bill : bills) {
			if (bill.getTransactionPrice() > highestPrice) {
				highestPrice = bill.getTransactionPrice();
			}
		}
		System.out.println("今天最高价："+highestPrice);
		
	}

}
