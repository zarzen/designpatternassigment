package zz.lab;

import java.util.Vector;

public interface Observer {
	public void update(Vector<Bill> bills);
}
